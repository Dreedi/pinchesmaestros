from django.conf.urls import patterns, url, include

urlpatterns = patterns('', 
	url(r'^logout/$', 'django_social_app.views.session_logout', name='logout'),
)