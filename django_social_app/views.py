from django.contrib.auth import logout as auth_logout
from django.core.urlresolvers import reverse
from django.shortcuts import redirect

def session_logout(request):
	auth_logout(request)
	return redirect(reverse('home'))