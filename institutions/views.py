from django.views.generic import View, TemplateView

from students.models import Student

class HomeView(TemplateView):
	template_name = 'home.html'

	def get_context_data(self):
		context = super(HomeView, self).get_context_data()
		context['title'] = 'Pinches Maestros'
		context['meta_description'] = 'Pinches Maestros es el lugar donde se escriben las verdades de tus profesores'
		if not self.request.user.is_anonymous():
			student_object = Student.objects.get_or_create(user=self.request.user)
			if not student_object[0].name or not student_object[0].avatar:
				student_object[0].fill_data()
			context['student'] = student_object[0]
		return context