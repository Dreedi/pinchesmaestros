from django.db import models

class Institution(models.Model):
	name = models.CharField(max_length=700, unique=True)
	short_name = models.CharField(max_length=700, null=True)
	logo = models.ImageField(upload_to='institutions_logos', null=True)

	def __unicode__(self):
		return self.name

	def show_logo(self):
		return """
			<img src="%s" style="display: block; width: 100px;" />""" %self.logo.url
	show_logo.allow_tags = True

class School(models.Model):
	name = models.CharField(max_length=700)
	short_name = models.CharField(max_length=700, null=True)
	institution = models.ForeignKey(Institution, null=True)
	logo = models.ImageField(upload_to='institutions_logos', null=True)

	def __unicode__(self):
		return self.name

	def show_logo(self):
		return """
			<img src="%s" style="display: block; width: 100px;" />""" %self.logo.url
	show_logo.allow_tags = True