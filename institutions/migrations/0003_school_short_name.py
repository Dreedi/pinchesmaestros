# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('institutions', '0002_institution_short_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='school',
            name='short_name',
            field=models.CharField(max_length=700, null=True),
        ),
    ]
