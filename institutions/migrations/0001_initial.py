# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Institution',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=700)),
                ('logo', models.ImageField(null=True, upload_to=b'institutions_logos')),
            ],
        ),
        migrations.CreateModel(
            name='School',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=700)),
                ('logo', models.ImageField(null=True, upload_to=b'institutions_logos')),
                ('institution', models.ForeignKey(to='institutions.Institution', null=True)),
            ],
        ),
    ]
