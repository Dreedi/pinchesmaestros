from django.contrib import admin

from .models import Institution, School

@admin.register(Institution)
class InstitutionAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'show_logo')

@admin.register(School)
class SchoolAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'show_logo')