from django.db import models
from django.contrib.auth.models import User

import requests

class Student(models.Model):
	user = models.OneToOneField(User, unique=True)
	name = models.CharField(max_length=300, null=True, blank=True)
	avatar = models.CharField(max_length=1000, null=True, blank=True)

	def fill_data(self):
		user_id = self.user.social_auth.get(provider='facebook').uid
		facebook_access_token = self.user.social_auth.get(provider='facebook').extra_data['access_token']
		name_url = 'https://graph.facebook.com/me/?fields=name&access_token=%s' %facebook_access_token
		name = requests.get(name_url).json()['name']
		picture_url = 'https://graph.facebook.com/%s/picture/?width=500&height=500' %user_id
		self.name = name
		self.avatar = picture_url
		return self.save()

	def show_avatar(self):
		return """<img src="%s" style="display: block; width: 100px;" />""" %self.avatar

	show_avatar.allow_tags = True